$(document).ready(function () {
	obtenerTareas();
	let modificar = false;
	function obtenerTareas() {
		$.ajax({
			url: 'listar.php',
			type: "GET",
			success: function (response) {
				let tasks = JSON.parse(response);
				let template = '';
				tasks.forEach(task => {
					template += `
					<tr taskId="${task.id}">
						<td>
							<p >${task.name} </p>
						</td>
						<td>${task.description}</td>
						<td>
							<button class="task-delete btn btn-outline-danger">ELIMINAR</button>
						</td>
						<td>
							<button data-bs-toggle="modal" data-bs-target="#exampleModal" class="task-item btn btn-outline-primary">EDITAR</button>
						</td>
					</tr>
					`
				}); 
				$('#tasks').html(template);
			}
		});
	}

	$('#task-form').submit(e => {
		const datos = {
			name: $('#name').val(),
			description: $('#description').val(),
			id: $('#taskId').val()
		};

		const url = modificar === false ? 'insertar.php' : 'modificar.php';

		$.post(url, datos, (response) => {
			obtenerTareas();
		});
	});

	$(document).on('click', '.task-item', (e) => {
		const elemento = $(this)[0].activeElement.parentElement.parentElement;
		const id = $(elemento).attr('taskId');
		console.log(id);
		$.post('getTarea.php', { id }, (response) => {
			const task = JSON.parse(response);
			$('#name').val(task.name);
			$('#description').val(task.description);
			$("#exampleModalLabel").text("EDITAR DATOS");
			$('#taskId').val(task.id);
			modificar = true;
		});
	});

	$(document).on('click', '.task-delete', (e) => {
		if (confirm("Desea eliminar a esta persona?")) {
			const elemento = $(this)[0].activeElement.parentElement.parentElement;
			const id = $(elemento).attr('taskId');
			$.post('eliminar.php', { id }, (response) => {
				obtenerTareas();
			});
		}
	});
	$(document).on('click', '.guardaDatos', (e) => {
			$("#exampleModalLabel").text("NUEVO REGISTRO");
			$('#name').val('');
			$('#description').val('');
			$('#taskId').val('');
			modificar = false;
	});
});